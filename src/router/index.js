import Vue from 'vue'
import VueRouter from 'vue-router'
import CampusBinhai from '../views/CampusBinhai.vue'
import CampusChashan from '../views/CampusChashan.vue'
import CampusYueqing from '../views/CampusYueqing.vue'
import StudentSystem from '../views/StudentSystem.vue'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: CampusBinhai
  },
  {
    path: '/CampusChashan',
    name: 'CampusChashan',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: CampusChashan,
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/CampusChashan.vue')
    }
  },
  {
    path: '/CampusYueqing',
    name: 'CampusYueqing',
    component: CampusYueqing
  },
  {
    path: '/student',
    name: 'student',
    component: StudentSystem,
    children:[{path:'/Login',component:Login}]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
